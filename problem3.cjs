function sortedAllCarsMaker(data){
    const arr = [];
    if(!Array.isArray(data)) {
        return arr;
    }
    if(data.length==0) {
        return arr;
    }
    return data.map(models => models.car_model).sort();
}

module.exports = sortedAllCarsMaker;