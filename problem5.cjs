const oldCarsList = (data) => {
    const arr = [];
    if(!Array.isArray(data)) {
        return arr;
    }
    if(data.length==0) {
        return arr;
    }
    return data.filter(year => year < 2000).length;
}

module.exports = oldCarsList;