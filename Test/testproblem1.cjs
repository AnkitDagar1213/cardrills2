const findById = require('../problem1.cjs')
const data = require('../information.cjs')

const car = findById(data,33);
console.log(`Car ${car[0].id} is a ${car[0].car_year} ${car[0].car_make} ${car[0].car_model}`);