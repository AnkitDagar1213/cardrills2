const listOfYears = (data) => {
    const arr = [];
    if(!Array.isArray(data)) {
        return arr;
    }
    if(data.length==0) {
        return arr;
    }
    return data.map(models => models.car_year).sort();
}

module.exports = listOfYears;
