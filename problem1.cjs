function findById(data,Id){
    const arr = [];
    if(!Array.isArray(data)) {
        return arr;
    }
    if(data.length==0) {
        return arr;
    }
    if(typeof Id !== 'number'){
        return arr;
    }
    data=data.filter(ids => ids.id===Id)
    return data
}

module.exports = findById;