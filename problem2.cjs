function lastCar(data){
    const arr = [];
    if(!Array.isArray(data)) {
        return arr;
    }
    if(data.length==0) {
        return arr;
    }
    return data.filter((obj) => (obj.id == data.length));
}

module.exports = lastCar;