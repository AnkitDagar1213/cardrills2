const onlyBMWAndAudi = (data) => {
    const arr = [];
    if(!Array.isArray(data)) {
        return arr;
    }
    if(data.length==0) {
        return arr;
    }
    const ans = data.filter(item => item.car_make == "BMW" || item.car_make == "Audi");
    const cars = JSON.stringify(ans,null,2)
    return cars
}
module.exports = onlyBMWAndAudi;
